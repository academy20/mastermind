package it.prismaprogetti.mastermind.utility;

public class Utility {

	public static String convertiMillisecondi(long millisecondi) {

		long durata = millisecondi;
		long ore = durata / (1000 * 60 * 60);
		durata -= (ore * 1000 * 60 * 60);
		long minuti = durata / (1000 * 60);
		durata -= (minuti * 1000 * 60);
		long secondi = durata / 1000;
		return ore + "h," + minuti + "m," + secondi + "s";
	}
}
