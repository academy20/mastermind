package it.prismaprogetti.mastermind.model;

import java.util.Scanner;

import it.prismaprogetti.mastermind.error.NumeroDiColoriException;

public class NumeroDiColori {

	private final int numeroColori;

	private NumeroDiColori(int numeroColori) {
		super();
		this.numeroColori = numeroColori;
	}

	public int getNumeroDiColori() {
		return numeroColori;
	}

	/*
	 * controllo che l'utente inserisca i valori da 2 a 6
	 */
	static NumeroDiColori checkNumeroDiColori(Scanner scanner) {

		NumeroDiColori numColori = null;
		boolean controlloCheSiaUnNumero = false;
		String datoRicevuto = null;

		while (controlloCheSiaUnNumero == false) {
			System.out.println("Numero di Colori? (2-6)");
			datoRicevuto = scanner.nextLine();

			if (datoRicevuto.matches("-?\\d+") && Integer.parseInt(datoRicevuto) >= 2
					&& Integer.parseInt(datoRicevuto) <= 6) {
				controlloCheSiaUnNumero = true;
				numColori = new NumeroDiColori(Integer.parseInt(datoRicevuto));
			} else {
				System.out.println(NumeroDiColoriException.inserisciUnNumeroColoriValido());
			}

		}
		return numColori;
	}

}
