package it.prismaprogetti.mastermind.model;

import java.util.Scanner;

import it.prismaprogetti.mastermind.error.MenuException;

/**
 * classe che contiene il menu iniziale del gioco 
 * @author alessandro.izzo
 *
 */
public class Menu {

	public static boolean avvia(Scanner scanner) {
		String datoRicevuto = null;
		boolean gioca = false;

		System.out.println("Benvenuto nel MASTERMIND" + "\n");
		while (gioca != true) {
			
			System.out.println("Digita H per Help");
			System.out.println("Digita A per avvio partita");
			System.out.println("Digita Q per uscire");
			datoRicevuto = scanner.nextLine();
		
			if (datoRicevuto.equalsIgnoreCase("H") || datoRicevuto.equalsIgnoreCase("A")
					|| datoRicevuto.equalsIgnoreCase("Q") && datoRicevuto.length() == 1) {
				
				
			}else {
				System.out.println(MenuException.inserisciUnCarattereValido());
			}
			
			
			
			if (datoRicevuto.equalsIgnoreCase("H")) {
				System.out.println("https://it.wikipedia.org/wiki/Mastermind");
			}

			if (datoRicevuto.equalsIgnoreCase("Q")) {
				System.out.println("Gioco Terminato");
				break;
			}

			if (datoRicevuto.equalsIgnoreCase("A")) {
				System.out.println("Buona Partita");
				gioca = true;

			}

		}

		

		return gioca;
	}
}
