package it.prismaprogetti.mastermind.model;

import java.util.Scanner;

import org.apache.commons.lang3.time.StopWatch;

import it.prismaprogetti.mastermind.error.CodiceDaIndovinareException;
import it.prismaprogetti.mastermind.utility.Utility;

public class Mastermind {

	/*
	 * classe che funger� da scanner
	 */

	public static void gioca() throws CodiceDaIndovinareException {

		Scanner scanner = new Scanner(System.in);
		/*
		 * avvio il menu di gioco
		 */
		boolean gioca = Menu.avvia(scanner);

		if (gioca == true) {

			/*
			 * prendo i dati che mi servono per giocare -numero dei tentativi -numero dei
			 * colori(numeri da usare) -grandezza della combinazione da indovinare
			 */
			int numeroTentativi = NumeroDiTentativi.checkNumeroDiTentativi(scanner).getNumeroDiTentativi();
			int numeroColori = NumeroDiColori.checkNumeroDiColori(scanner).getNumeroDiColori();
			int numeroPosizioni = NumeroDiPosizioni.chiediNumeroDiPosizioni(scanner).getNumeroDiPosizioni();
			System.out.println(
					"-----------------------------------------------------------------------------------------");
			System.out.println("Numero Tentativi: " + numeroTentativi);
			System.out.println("Numero Colori: " + numeroColori);
			System.out.println("Numero Posizioni: " + numeroPosizioni);
			System.out.println("BUONA PARTITA!");
			System.out.println(
					"-----------------------------------------------------------------------------------------");

			/*
			 * genero il codice da indovinare
			 */
			CodiceDaIndovinare codiceDaIndovinare = CodiceDaIndovinare.crea(numeroColori, numeroPosizioni);

			/*
			 * boolean per il 1* ciclo WHILE
			 */
			boolean codiciConfrontati = false;
			boolean checkChiusuraGioco = false;
			/*
			 * array con il codice generato
			 */
			int[] codiceDaIndovinareTemporaneo = new int[codiceDaIndovinare.getCodice().length];

			/*
			 * contatore per i tentativi
			 */
			int contatoreTentativi = 1;
			/*
			 * preparo l'eventuale codiceIndovinato
			 */
			CodiceIndovinato codiceIndovinato = null;

			/*
			 * esco da questo while quando l'utente termina i tentativi o se ha indovinato
			 * il codice
			 */

			/*
			 * parte il tempo di gioco
			 */
			StopWatch stopWatch = new StopWatch();
			System.out.println("Timer di gioco partito");
			stopWatch.start();
			
			while (checkChiusuraGioco == false) {

//				/*
//				 * sposto il codice da indovinare in un array temporaneo
//				 */
//				for (int i = 0; i < codiceDaIndovinareTemporaneo.length; i++) {
//					int numeroDaIndovinare = codiceDaIndovinare.getCodice()[i];
//					codiceDaIndovinareTemporaneo[i] = numeroDaIndovinare;
//
//				}
				codiceDaIndovinareTemporaneo = codiceDaIndovinare.getCodice().clone();
				

				/*
				 * stringa che stampa il numero dei tentativi e le varie informazioni
				 */
				String tentativi = "tentativo" + contatoreTentativi + "/" + numeroTentativi + ". Inserisci i "
						+ numeroPosizioni + " colori da 1 a " + numeroColori + " separati da virgola";

				/*
				 * esco da questo while quando l'utente inserisce un codice valido
				 */
				CodiceInseritoDallUtente codiceInserito = null;
				System.out.println(tentativi);
				while (codiceInserito == null) {
					
					codiceInserito = CodiceInseritoDallUtente.checkCodiceInserito(scanner, numeroColori,
							numeroPosizioni, numeroTentativi, contatoreTentativi);
				}

				/*
				 * prendo il codice che ha inserito l'utente
				 */
				int[] codiceInseritoArray = codiceInserito.getCodiceInserito();

				/*
				 * esco da questo while quando ho finito di controllare il codice che ho
				 * generato con il codice che ha inserito l'utente
				 */
				codiciConfrontati = false;
				while (codiciConfrontati == false) {

					/*
					 * array con le posizioni indovinate
					 */
					int[] codiceIndovinatoArray = new int[numeroPosizioni];

					/*
					 * stringa che conterr� + o - + = numero e posizione indovinata. - = numero
					 * indovinato ma non la posizione
					 */
					String coloriIndovinati = "";

					/*
					 * contatore dei colori che l'utente ha indovinato
					 */
					int contatoreColoriIndovinati = 0;

					coloriIndovinati = ControllaCodici.controllaColoriNellaStessaPosizione(codiceDaIndovinareTemporaneo,
							codiceInseritoArray, codiceIndovinatoArray, coloriIndovinati, contatoreColoriIndovinati);

					coloriIndovinati = ControllaCodici.controllaColoriUgualiMaPosizioneDiversa(
							codiceDaIndovinareTemporaneo, codiceInseritoArray, codiceIndovinatoArray, coloriIndovinati,
							contatoreColoriIndovinati);

					contatoreColoriIndovinati = ControllaCodici.controllaColoriIndovinati(coloriIndovinati,
							contatoreColoriIndovinati);

					if (contatoreColoriIndovinati == numeroPosizioni) {
						codiceIndovinato = new CodiceIndovinato(codiceIndovinatoArray);
						checkChiusuraGioco = true;
						codiciConfrontati = true;
					}

					/*
					 * setto codiciConfrontati a true cosi, esco dal ciclo e l'utente pu� inserire
					 * una nuova combinazione
					 */
					if (contatoreTentativi < numeroTentativi) {

						System.out.println("-----------------------------------------------");
						System.out.println("Esito Tentativo " + contatoreTentativi + ": " + coloriIndovinati);
						System.out.println("-----------------------------------------------");

						codiciConfrontati = true;

					}

					/*
					 * se ho raggiungo i tentativi massimi, setto tutto a true ed esco dal gioco
					 */
					if (contatoreTentativi == numeroTentativi) {
						checkChiusuraGioco = true;
						codiciConfrontati = true;
					}
					contatoreTentativi++;
				}

			}

			/*
			 * se codice indovinato non � null, l'utente ha indovinato. Stoppo il tempo,
			 * converto i millisecondi in ore,minuti,secondi
			 */
			if (codiceIndovinato != null) {
				stopWatch.stop();
				String tempo = Utility.convertiMillisecondi(stopWatch.getTime());
				System.out.println("Complimenti hai indovinato il codice in: " + tempo
						+ " . Il codice da indovinare era: " + codiceDaIndovinare.toString());
			} else {
				System.out.println("Mi dispiace, hai terminato i tentativi disponibili. Il codice era: "
						+ codiceDaIndovinare.toString());
				System.out.println("Partita Terminata");
			}

		}

	}
}
