package it.prismaprogetti.mastermind.model;

import java.util.Arrays;

import it.prismaprogetti.mastermind.error.CodiceDaIndovinareException;

/**
 * SINGLETON
 */
public class CodiceDaIndovinare implements Cloneable{

	private static CodiceDaIndovinare codiceDaIndovinare;
	private int[] codice;

	private CodiceDaIndovinare(int[] codice) {
		this.codice = codice;
	}

	public static CodiceDaIndovinare getCodiceDaIndovinare() {
		return codiceDaIndovinare;
	}

	public int[] getCodice() {
		return codice;
	}

	@Override
	public String toString() {
		return Arrays.toString(codice);
	}

	/**
	 * genero un codice da indovinare in base a quanti colori ho e quanti colori posso mettere
	 * @param numeroDiColori
	 * @param numeroDiPosizioni
	 * @return
	 * @throws CodiceDaIndovinareException
	 */
	static CodiceDaIndovinare crea(int numeroDiColori, int numeroDiPosizioni)
			throws CodiceDaIndovinareException {

		/*
		 * verifico che siano numeri che rientrano nei dati pre-stabiliti
		 */
		checkValiditaNumeri(numeroDiColori, numeroDiPosizioni);

		/*
		 * se non esiste nessuna istanza ne creo una, altrimento torno quella che gi� esiste
		 */
		if (codiceDaIndovinare == null) {
			int[] codice = new int[numeroDiPosizioni];

			for (int i = 0; i < codice.length; i++) {
				int numeroRandom = (int) (Math.random() * numeroDiColori + 1);
				codice[i] = numeroRandom;
			}
			codiceDaIndovinare = new CodiceDaIndovinare(codice);
		}

		return codiceDaIndovinare;

	}

	private static void checkValiditaNumeri(int numeroDiColori, int numeroDiPosizioni)
			throws CodiceDaIndovinareException {

		if (numeroDiColori >= 2 && numeroDiColori <= 6 && numeroDiPosizioni >= 3 && numeroDiPosizioni <= 6) {

		} else {
			throw CodiceDaIndovinareException.numeriNonValidi();
		}

	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
}
