package it.prismaprogetti.mastermind.model;

import java.util.Scanner;

import it.prismaprogetti.mastermind.error.NumeroDiTentativiException;

public class NumeroDiTentativi {

	private final int numeroDiTentativi;

	private NumeroDiTentativi(int numeroDiTentativi) {
		super();
		this.numeroDiTentativi = numeroDiTentativi;
	}

	public int getNumeroDiTentativi() {
		return numeroDiTentativi;
	}

	/*
	 * controllo che l'utente inserisca un valore da 1 a 12
	 */
	static NumeroDiTentativi checkNumeroDiTentativi(Scanner scanner) {
		NumeroDiTentativi numTentativi = null;
		boolean controlloCheSiaUnNumero = false;
		String datoRicevuto = null;

		while (controlloCheSiaUnNumero == false) {
			System.out.println("Numero di tentativi? (1-12)");
			datoRicevuto = scanner.nextLine();

			if (datoRicevuto.matches("-?\\d+") && Integer.parseInt(datoRicevuto) >= 1
					&& Integer.parseInt(datoRicevuto) <= 12) {
				controlloCheSiaUnNumero = true;
				numTentativi = new NumeroDiTentativi(Integer.parseInt(datoRicevuto));
			} else {
				System.out.println(NumeroDiTentativiException.inserisciUnNumeroValido());
			}

		}

		return numTentativi;
	}
}
