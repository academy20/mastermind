package it.prismaprogetti.mastermind.model;

import java.util.Scanner;


import it.prismaprogetti.mastermind.error.NumeroDiPosizioniException;

public class NumeroDiPosizioni {

	private final int numeroPosizioni;

	private NumeroDiPosizioni(int numeroPosizioni) {
		super();
		this.numeroPosizioni = numeroPosizioni;
	}

	public int getNumeroDiPosizioni() {
		return numeroPosizioni;
	}

	/*
	 * controllo che l'utente inserisca i valori da 3 a 6
	 */
	static NumeroDiPosizioni chiediNumeroDiPosizioni(Scanner scanner) {

		NumeroDiPosizioni numPosizioni = null;
		boolean controlloCheSiaUnNumero = false;

		String datoRicevuto = null;

		while (controlloCheSiaUnNumero == false) {
			System.out.println("Numero di Posizioni? (3-6)");
			datoRicevuto = scanner.nextLine();

			if (datoRicevuto.matches("-?\\d+") && Integer.parseInt(datoRicevuto) >= 3
					&& Integer.parseInt(datoRicevuto) <= 6) {
				
				controlloCheSiaUnNumero = true;
				numPosizioni = new NumeroDiPosizioni(Integer.parseInt(datoRicevuto));
			} else {
				System.out.println(NumeroDiPosizioniException.inserisciUnNumeroPosizioniValido());
			}

		}
		return numPosizioni;
	}

}
