package it.prismaprogetti.mastermind.model;

import java.util.Arrays;

/**
 * classe che conterr� il codice Indovinato
 * 
 * @author alessandro.izzo
 *
 */
public class CodiceIndovinato {

	private int[] codiceIndovinato;

	CodiceIndovinato(int[] codiceIndovinato) {
		super();
		this.codiceIndovinato = codiceIndovinato;
	}

	public int[] getCodiceInserito() {
		return codiceIndovinato;
	}

	@Override
	public String toString() {
		return "CodiceIndovinato [codiceIndovinato=" + Arrays.toString(codiceIndovinato) + "]";
	}

}
