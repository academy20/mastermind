package it.prismaprogetti.mastermind.model;

import java.util.Arrays;
import java.util.Scanner;

import it.prismaprogetti.mastermind.error.CodiceInseritoDallUtenteException;

public class CodiceInseritoDallUtente {

	private int[] codiceInserito;

	private CodiceInseritoDallUtente(int[] codiceInserito) {
		super();
		this.codiceInserito = codiceInserito;
	}

	public int[] getCodiceInserito() {
		return codiceInserito;
	}

	@Override
	public String toString() {
		return "CodiceInserito [codiceInserito=" + Arrays.toString(codiceInserito) + "]";
	}

	/**
	 * controllo il codice inserito
	 */
	static CodiceInseritoDallUtente checkCodiceInserito(Scanner scanner, int numeroColori,
			int numeroPosizioni, int numeroTentativi, int contatoreTentativi) {

		/*
		 * inizializzo l'oggetto da tornare
		 */
		CodiceInseritoDallUtente codiceInseritoDallUtenteObj = null;
		int[] codiceInserito = new int[numeroPosizioni];

		int contatoreColoriControllati = 0;
		String codiceInseritoDallUtente = null;

		while (codiceInseritoDallUtenteObj == null) {
			codiceInseritoDallUtente = scanner.nextLine();
			/*
			 * soluzione Bug trovato da Flavio
			 * con questo controllo elimino tutte le possibili combinazioni che non siano
			 * numeri + virgola
			 */
			if (codiceInseritoDallUtente.length() != (numeroPosizioni * 2) - 1) {
				System.out.println(CodiceInseritoDallUtenteException.inserisciUnCodiceValido(contatoreTentativi,
						numeroTentativi, numeroPosizioni, numeroColori));
				break;
			}
			
			/*
			 * commento momentaneamente perch� credo che non serva pi� con il controllo che fa sopra
			 * ora mi serve sapere solo se sono numeri validi, qualsiasi tipo 
			 */
			//codiceInseritoDallUtente = codiceInseritoDallUtente.replaceAll("\\s+", "");

			String[] datoRicevutoArray = codiceInseritoDallUtente.split(",");

			/*
			 * verifico che ci siano tanti numeri quanti ne deve inserire l'utente
			 */
			if (datoRicevutoArray.length == numeroPosizioni) {

				for (int i = 0; i < datoRicevutoArray.length; i++) {
					String string = datoRicevutoArray[i];

					/*
					 * se becco un cassetto vuoti,blocco e torno l'errore
					 */
					if (string.equals("")) {
						System.out.println(CodiceInseritoDallUtenteException.inserisciUnCodiceValido(contatoreTentativi,
								numeroTentativi, numeroPosizioni, numeroColori));
						break;
					}

					/*
					 * se il numero che becco � uguale o inferiori al massimo che poteva inserire
					 * l'utente ok, altrimenti lancio errore
					 */
					if (Integer.parseInt(string) <= numeroColori) {
						contatoreColoriControllati++;
					} else {
						System.out.println(CodiceInseritoDallUtenteException.inserisciUnCodiceValido(contatoreTentativi,
								numeroTentativi, numeroPosizioni, numeroColori));
						break;
					}
				}
			}

			/*
			 * se il contatore raggiunge la quantit� di numeri da cui doveva 
			 * essere composto il codice, creo il codiceInseritoDallUtente
			 */
			if (contatoreColoriControllati == numeroPosizioni) {

				for (int i = 0; i < datoRicevutoArray.length; i++) {
					codiceInserito[i] = Integer.parseInt(datoRicevutoArray[i]);

				}

				/*
				 * arrivati qui, la variabile non � pi� null quindi esco dal ciclo while
				 * e torno l'oggetto
				 */
				codiceInseritoDallUtenteObj = new CodiceInseritoDallUtente(codiceInserito);
			}
		}

		return codiceInseritoDallUtenteObj;
	}
}
