package it.prismaprogetti.mastermind.model;

public class ControllaCodici {

	
	public static String controllaColoriNellaStessaPosizione(int[] codiceDaIndovinare, int[] codiceInseritoDallUtente,
			int[] codiceIndovinatoArray, String coloriIndovinati, int contatoreColoriIndovinati) {

		for (int i = 0; i < codiceDaIndovinare.length; i++) {
			int coloreDelComputer = codiceDaIndovinare[i];

			for (int j = i; j < codiceInseritoDallUtente.length; j++) {
				int coloreDelUtente = codiceInseritoDallUtente[j];

				/*
				 * se sono numeri uguali e nella stessa posizione copio il numero nell'array dei
				 * numeri indovinati nella posizione in cui � stato trovato e metto uno 0 al
				 * posto del numero che ho indovinato nell array del codice che ho generato e
				 * nell'array del codice dell'utente
				 */
				if (coloreDelComputer == coloreDelUtente && i == j) {
					coloriIndovinati = coloriIndovinati + "+";
					codiceIndovinatoArray[j] = coloreDelComputer;
					codiceInseritoDallUtente[i] = 0;
					codiceDaIndovinare[i] = 0;
					contatoreColoriIndovinati++;
					break;
				}

			}

		}

		return coloriIndovinati;
	}

	public static String controllaColoriUgualiMaPosizioneDiversa(int[] codiceDaIndovinare,
			int[] codiceInseritoDallUtente, int[] codiceIndovinatoArray, String coloriIndovinati,
			int contatoreColoriIndovinati) {

		for (int i = 0; i < codiceDaIndovinare.length; i++) {
			int coloreDelComputer = codiceDaIndovinare[i];

			for (int j = 0; j < codiceInseritoDallUtente.length; j++) {

				int coloreDelUtente = codiceInseritoDallUtente[j];

				/*
				 * se becco uno 0 significa che il numero � gi� stato indovinato in quella
				 * posizione
				 */
				if (coloreDelComputer == 0) {
					break;
				}
				/*
				 * quando trovo un numero uguale metto un - e levo quel numero dagli array di
				 * controllo
				 */
				if (coloreDelComputer == coloreDelUtente) {
					coloriIndovinati = coloriIndovinati + "-";
					codiceInseritoDallUtente[j] = 0;
					codiceDaIndovinare[i] = 0;
					break;

				}

			}

		}

		return coloriIndovinati;
	}

	public static int controllaColoriIndovinati(String coloriIndovinati, int contatoreColoriIndovinati) {

		char[] coloriChar = coloriIndovinati.toCharArray();

		for (int i = 0; i < coloriChar.length; i++) {
			char c = coloriChar[i];

			if (c == '+') {
				contatoreColoriIndovinati++;
			}
		}

		return contatoreColoriIndovinati;
	}
}
