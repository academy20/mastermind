package it.prismaprogetti.mastermind.error;

import java.util.InputMismatchException;

public class NumeroDiPosizioniException extends InputMismatchException {

	private NumeroDiPosizioniException(String message) {
		super(message);
	}

	public static String inserisciUnNumeroPosizioniValido() {
		return new NumeroDiPosizioniException(
				"Numero inserito non valido, inserisci un numero tra 3 e 6 per proseguire").getMessage();
	}
}
