package it.prismaprogetti.mastermind.error;

import java.util.InputMismatchException;

public class NumeroDiTentativiException extends InputMismatchException {

	private NumeroDiTentativiException(String message) {
		super(message);
	}

	public static String inserisciUnNumeroValido() {
		return new NumeroDiTentativiException(
				"Numero inserito non valido, inserisci un numero tra 1 e 12 per proseguire").getMessage();
	}
	
	
}
