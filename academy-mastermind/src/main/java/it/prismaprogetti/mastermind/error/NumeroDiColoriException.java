package it.prismaprogetti.mastermind.error;

import java.util.InputMismatchException;

public class NumeroDiColoriException extends InputMismatchException {

	private NumeroDiColoriException(String message) {
		super(message);
	}

	public static String inserisciUnNumeroColoriValido() {
		return new NumeroDiColoriException(
				"Numero inserito non valido, inserisci un numero tra 2 e 6 per proseguire").getMessage();
	}
}
