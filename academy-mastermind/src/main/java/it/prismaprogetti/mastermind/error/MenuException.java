package it.prismaprogetti.mastermind.error;

import java.util.InputMismatchException;

public class MenuException extends InputMismatchException {

	
	private MenuException(String message) {
		super(message);
	}
	
	public static String inserisciUnCarattereValido() {
		return new MenuException("Scrivi una lettera tra H,A,Q").getMessage();
	}
}
