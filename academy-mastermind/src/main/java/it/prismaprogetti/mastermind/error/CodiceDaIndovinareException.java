package it.prismaprogetti.mastermind.error;

public class CodiceDaIndovinareException extends Exception{

	
	private CodiceDaIndovinareException(String message) {
		super(message);
	}
	
	
	public static CodiceDaIndovinareException numeriNonValidi() {
		return new CodiceDaIndovinareException("i numeri inseriti non solo validi, hai provato a barare ehhhh");
	}
}
