package it.prismaprogetti.mastermind.error;

import java.util.InputMismatchException;

public class CodiceInseritoDallUtenteException extends InputMismatchException {

	private CodiceInseritoDallUtenteException(String message) {
		super(message);
	}

	public static String inserisciUnCodiceValido(int contatoreTentativi, int numeroTentativi, int numeroPosizioni,
			int numeroColori) {
		return new CodiceInseritoDallUtenteException("tentativo" + contatoreTentativi + "/" + numeroTentativi
				+ ". Inserisci i " + numeroPosizioni + " colori da 1 a " + numeroColori + " separati da virgola")
				.getMessage();
	}
}
