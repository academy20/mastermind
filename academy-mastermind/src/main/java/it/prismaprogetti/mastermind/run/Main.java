package it.prismaprogetti.mastermind.run;

import it.prismaprogetti.mastermind.error.CodiceDaIndovinareException;
import it.prismaprogetti.mastermind.model.Mastermind;

public class Main {

	public static void main(String[] args) throws CodiceDaIndovinareException {

		Mastermind.gioca();

	}

}
